package com.example.verizon_assessment;

import android.content.Context;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.getkeepsafe.relinker.ReLinker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HiJNI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hi_jni2);
        retrieveHiProgram();
    }
    private native String sayHi();

    public static void main(String[] args) {

    }

    ReLinker.Logger logger = new ReLinker.Logger() {
        @Override
        public void log(String message) {
            //log(message);
        }
    };
    public void retrieveHiProgram () {
        ReLinker.log(logger)
                .force()
                .recursively()
                .loadLibrary(HiJNI.this, "com_example_verizon_assessment_HiJNI");
        Button button = (Button) findViewById(R.id.button);
        final ListView lv = (ListView) findViewById(R.id.lv);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtForListView = new HiJNI().sayHi();
                String txtExploded[] = txtForListView.split(" ");
                /*for(int i=0; i<txtExploded.length; i++){
                    Arraylist<EditValue>
                }*/
                final List<String> exploded = new ArrayList<String>(Arrays.asList(txtExploded));
                final ArrayAdapter<String> arrAdapter;
                arrAdapter = new ArrayAdapter<String>(HiJNI.this, android.R.layout.simple_list_item_1, exploded);
                lv.setAdapter(arrAdapter);

            }
        });
    }
        //ReLinker.loadLibrary(this.Context, "com_example_verizon_assessment_HiJNI");

}
