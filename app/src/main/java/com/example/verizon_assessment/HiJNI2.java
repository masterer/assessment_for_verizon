package com.example.verizon_assessment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.getkeepsafe.relinker.ReLinker;

public class HiJNI2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hi_jni2);
    }
    private native String sayHi();

    ReLinker.Logger logger = new ReLinker.Logger() {
        @Override
        public void log(String message) {
            log(message);
        }
    };
    private void retrieveHiProgram () {
        ReLinker.log(logger)
                .force()
                .recursively()
                .loadLibrary(HiJNI2.this, "com_example_verizon_assessment_HiJNI");
    }
}
